using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchTransform : MonoBehaviour
{
    public Transform target;
    public Vector3 targetLocalFollowOffset;

    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        // Position
        if (rb != null) rb.MovePosition(target.TransformPoint(targetLocalFollowOffset));
        else transform.position = target.TransformPoint(targetLocalFollowOffset);
        
        // Rotation
        Quaternion rot = Quaternion.LookRotation(target.forward);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, .5f);
    }
}

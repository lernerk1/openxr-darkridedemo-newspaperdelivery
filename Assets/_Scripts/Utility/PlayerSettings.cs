using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerSettings
{
    public static float TravelSpeed
    {
        get => PlayerPrefs.GetFloat(nameof(TravelSpeed));
        set
        {
            PlayerPrefs.SetFloat(nameof(TravelSpeed), value);
            PlayerPrefs.Save();
        }
    }

    public static float Vignette
    {
        get => PlayerPrefs.GetFloat(nameof(Vignette));
        set
        {
            PlayerPrefs.SetFloat(nameof(Vignette), value);
            PlayerPrefs.Save();
        }
    }

    public static string Route
    {
        get => PlayerPrefs.GetString(nameof(Route));
        set
        {
            PlayerPrefs.SetString(nameof(Route), value);
            PlayerPrefs.Save();
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedScaleAdjust : MonoBehaviour
{
    [field: SerializeField] public float Duration { get; private set; } = 1f;
    [field: SerializeField] public Vector3 TargetScale { get; private set; } = Vector3.zero;
    
    private Vector3 initialScale;
    private float startAdjustTime;
    private float U => Mathf.Clamp01((Time.time - startAdjustTime) / Duration);
    
    public void DoAdjust()
    {
        Debug.Log("Start Adjust");
        initialScale = transform.localScale;
        startAdjustTime = Time.time;
        StartCoroutine(nameof(Adjust));
    }

    private IEnumerator Adjust()
    {
        while (U < 1)
        {
            transform.localScale = Vector3.Lerp(initialScale, TargetScale, U);
            yield return new WaitForEndOfFrame();
        }
        Debug.Log("End Adjust");
    }
}

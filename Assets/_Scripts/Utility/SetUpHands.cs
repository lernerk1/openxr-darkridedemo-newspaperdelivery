using System;
using System.Collections;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Management;

public class SetUpHands : MonoBehaviour
{
    public static SetUpHands S;

    public enum Hand { Left, Right }
    
    [SerializeField] private ActionBasedController left;
    [SerializeField] private ActionBasedController right;

    public Transform LeftTransform => left.transform;
    public Transform RightTransform => right.transform;
    
    public float LeftSelection => left.selectAction.action.ReadValue<float>();
    public float RightSelection => right.selectAction.action.ReadValue<float>();
    private void Awake()
    {
        if (S == null)
            S = this;
        else
        {
            Debug.Log("Multiple instances of SetUpHands, Destroying " + gameObject.name);
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        left.activateAction.action.performed += context => { };
        right.activateAction.action.performed += context => { };
    }


}

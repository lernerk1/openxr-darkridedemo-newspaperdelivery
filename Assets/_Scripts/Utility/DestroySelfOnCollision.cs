using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class DestroySelfOnCollision : MonoBehaviour
{
    [field: SerializeField] public float DestroyDelay { get; private set; } = 1f;
    public LayerMask targetLayers;
    public UnityEvent onCollision;

    private void DestroySelf()
    {
        Destroy(gameObject);
    }
    
    private void OnCollisionEnter(Collision other)
    {
        if (targetLayers == 0) return;
        Debug.Log(other.gameObject.name);
        if ((LayerMask.GetMask(LayerMask.LayerToName(other.gameObject.layer)) & targetLayers.value) != 0)
        {
            onCollision?.Invoke();
            Invoke(nameof(DestroySelf), DestroyDelay);
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class NewspaperHolster : MonoBehaviour
{
    [field: SerializeField] public Transform HolsterLocation { get; private set; }

    [field: SerializeField] public GameObject NewspaperPrefab { get; private set; }

    [field: SerializeField] public float SpawnCooldown { get; private set; } = 1f;

    public Rigidbody ActivePaper { get; private set; }
    private float spawnNext;
    
    // Start is called before the first frame update
    void Start()
    {
        spawnNext = 0;
        SpawnNewspaper();
    }
    
    void LateUpdate()
    {
        UpdateActivePaperTransform();
    }

    public void SpawnNewspaper()
    {
        if (NewspaperPrefab == null) return;
        ActivePaper = Instantiate(NewspaperPrefab, transform.parent).GetComponent<Rigidbody>();
        ActivePaper.gameObject.GetComponent<Newspaper>().Init(this);
        UpdateActivePaperTransform();
        spawnNext = Time.time + SpawnCooldown;
    }

    public void DelayedSpawnNewspaper()
    {
        Invoke(nameof(SpawnNewspaper), SpawnCooldown);
    }

    public void NullActiveNewspaper() => ActivePaper = null;

    private void UpdateActivePaperTransform()
    {
        if (ActivePaper == null) return;
        
        ActivePaper.position = HolsterLocation.position;
        ActivePaper.rotation = HolsterLocation.rotation;
        
        ActivePaper.velocity = Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(HolsterLocation.position, .075f * transform.lossyScale.magnitude);
    }
}

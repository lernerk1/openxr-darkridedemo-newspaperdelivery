using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using Random = UnityEngine.Random;

public class NewspaperThrow : MonoBehaviour
{
    public enum Hand { Left, Right }
    [Header("General")]
    [SerializeField] private Hand hand;
    [SerializeField] private Transform throwAnchor;

    [Header("Grab")] 
    [SerializeField] private float regrabRadius = .25f;
    [SerializeField] private LayerMask regrabLayer;
    
    [Header("Throw")]
    [SerializeField] private float throwForceMultiplier = 25f;
    
    public GameObject[] newspaperPrefabs;
    
    private SetUpHands hands;
    private GameObject newspaper;
    private Rigidbody rb;
    private Vector3 lastPos;
    private Transform handTransform => hand == Hand.Left ? hands.LeftTransform : hands.RightTransform;
    private float handSelection => hand == Hand.Left ? hands.LeftSelection : hands.RightSelection;

    private void Start()
    {
        hands = SetUpHands.S;
        lastPos = throwAnchor != null ? throwAnchor.position : transform.position;
    }

    private void FixedUpdate()
    {
        // Update position for velocity tracking 
        lastPos = throwAnchor != null ? throwAnchor.position : transform.position;
    }

    void LateUpdate()
    {
        if (handSelection > 0)
        {
            // check surrounding area for existing newspapers to regrab
            Collider[] results = Physics.OverlapSphere(handTransform.position,
                regrabRadius, regrabLayer);
            
            // if already grabbing a newspaper, update its position then exit
            if (newspaper != null)
            {
                Debug.Log("Grabbing " + newspaper.name);
                
                GrabMoveNewspaper();
            }
            else if (results.Length > 0)
            {
                var dist = float.MaxValue;
                var index = 0;
                for (int i = 0; i < results.Length; i++)
                {
                    var iDist = Vector3.Distance(handTransform.position, results[i].transform.position);
                    if (iDist >= dist) continue;
                    dist = iDist;
                    index = i;
                }

                newspaper = results[index].gameObject;
                
                rb = newspaper.GetComponent<Rigidbody>();
                
                Debug.Log("Regrabbing " + newspaper.name);
            }
            else
            {
                // instantiate new news paper
                newspaper = Instantiate(GetRandomNewspaper(), transform);
                GrabMoveNewspaper();
                
                rb = newspaper.GetComponent<Rigidbody>();
                rb.isKinematic = true;
                
                Debug.Log("New " + newspaper.name);
            }
        }
        else
        {
            if (newspaper != null)
            {
                rb.isKinematic = false;
                rb.velocity = (lastPos - (throwAnchor != null ? throwAnchor.position : transform.position)) * throwForceMultiplier / Time.deltaTime;
                
                newspaper = null;
                Debug.Log("Let go");
            }
        }
    }

    private GameObject GetRandomNewspaper()
    {
        return newspaperPrefabs[Random.Range(0, newspaperPrefabs.Length)];
    }
    
    private void GrabMoveNewspaper()
    {
        newspaper.transform.position = throwAnchor != null ? throwAnchor.position : transform.position;
        newspaper.transform.rotation = throwAnchor != null ? throwAnchor.rotation : transform.rotation;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(throwAnchor != null ? throwAnchor.position : transform.position, regrabRadius);
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandAnimator : MonoBehaviour
{
    public SetUpHands.Hand hand;
    
    private SetUpHands hands;

    private Animator anim; 
    
    // Start is called before the first frame update
    void Start()
    {
        hands = SetUpHands.S;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("Grip", hand == SetUpHands.Hand.Left ? hands.LeftSelection : hands.RightSelection);
    }
}

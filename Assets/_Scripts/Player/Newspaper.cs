using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Newspaper : MonoBehaviour
{
    [field: SerializeField] public byte Score { get; private set; } = 1;
    public bool Grabbed { get; private set; }
    public int GrabbedCount { get; private set; }
    
    private Rigidbody rb;
    private NewspaperHolster holster;
    
    
    private void AddScore() => ScoreManager.S.Score += Score;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        GrabbedCount = 0;
    }

    private void Update()
    {
        if (Grabbed)
        {
             // do vfx
             
             // play audio
             
        }
    }

    public void Init(NewspaperHolster newspaperHolster)
    {
        holster = newspaperHolster;
    }

    public void OnGrab()
    {
        Grabbed = true;
        if (GrabbedCount == 0) holster.NullActiveNewspaper();

        // do vfx

        // play audio

        GrabbedCount++;
    }

    public void OnRelease()
    {
        Grabbed = false;
        if (GrabbedCount <= 1) holster.DelayedSpawnNewspaper();

        // do vfx

        // play audio

    }

    public void OnScore()
    {
        // do vfx
        
        // do audio
        
        AddScore();
    }

    public void OnFail()
    {
        // do vfx
        
        // do audio
    }

    private void OnDestroy()
    {
        if (GrabbedCount == 0 && holster.ActivePaper == rb) holster.DelayedSpawnNewspaper();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEditor.Build.Content;
using UnityEngine.SceneManagement;

public class RouteSelectSystem : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown dropdown;

    private int index;
    
    // Start is called before the first frame update
    void Start()
    {
        index = dropdown.value;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateIndexToSelection()
    {
        index = dropdown.value;
    }

    public void IndexDown()
    {
        if (index > 0) index--;
        else index = dropdown.options.Count;
        dropdown.value = index;
    }
    
    public void IndexUp()
    {
        if (index < dropdown.options.Count - 1) index++;
        else index = 0;
        dropdown.value = index;
    }

    public void PlayRoute()
    {
        PlayerPrefs.SetInt("RouteSelected", index);
        SceneManager.LoadScene("RideTown");
    }
}

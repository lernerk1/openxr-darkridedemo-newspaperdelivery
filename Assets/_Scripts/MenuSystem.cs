using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MenuSystem : MonoBehaviour
{
    [Header("Primary Menu Element")]
    public GameObject primaryMenuElement;
    public void EnablePrimaryMenu()
    {
        primaryMenuElement.SetActive(true);
    }
    public void DisablePrimaryMenu()
    {
        primaryMenuElement.SetActive(false);
    }
    
    [Header("Route Select")]
    public GameObject routeSelectCanvas;
    public TMP_Text lapCountText;
    public Slider lapCount;
    public void UpdateLapCount()
    {
        lapCountText.text = lapCount.value.ToString();
        SaveSettings();
    }
    public void EnableRouteSelectMenu()
    {
        routeSelectCanvas.SetActive(true);
    }
    public void DisableRouteSelectMenu()
    {
        routeSelectCanvas.SetActive(false);
    }

    [Header("High Scores")] 
    public GameObject highScoreCanvas;
    public void EnableHighScoreCanvas()
    {
        highScoreCanvas.SetActive(true);
    }
    public void DisableHighScoreCanvas()
    {
        highScoreCanvas.SetActive(false);
    }
    
    [Header("Credits")]
    public GameObject creditsCanvas; 
    public void EnableCreditsCanvas()
    {
        creditsCanvas.SetActive(true);
    }
    public void DisableCreditsCanvas()
    {
        creditsCanvas.SetActive(false);
    }
    
    [Header("Settings")]
    public GameObject settingsCanvas;
    public Slider travelSpeed;
    public void SaveSettings()
    {
        PlayerSettings.TravelSpeed = travelSpeed.value;
        PlayerSettings.Vignette = 0f;
    }
    public void LoadSettings()
    {
        travelSpeed.value = PlayerSettings.TravelSpeed;
    }
    public void EnableSettingsMenu()
    {
        settingsCanvas.SetActive(true);
    }
    public void DisableSettingsMenu()
    {
        settingsCanvas.SetActive(false);
    }

    [Header("Quit")] 
    public bool doQuitInEditor = false;
    public void QuitApplication()
    {
        if (Application.isEditor)
        {
            if (doQuitInEditor) EditorApplication.ExitPlaymode();
        }
        else 
            Application.Quit();
    }
    private void OnApplicationQuit()
    {
        SaveSettings();
    }

    [Header("Pause")] 
    public GameObject pauseCanvas;
    public void EnablePauseMenu()
    {
        pauseCanvas.SetActive(true);
    }
    public void DisablePauseMenu()
    {
        pauseCanvas.SetActive(false);
    }
    
    // == GENERAL ==
    private void Start()
    {
        LoadSettings();
    }

    [Button]
    public void ResetSettings()
    {
        PlayerPrefs.DeleteAll();
        PlayerSettings.TravelSpeed = .75f;
        PlayerSettings.Vignette = 0f;
    }
    
    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void Resume()
    {
        Time.timeScale = 1;
    }
    
    public void ReturnToMainMenuScene()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void RestartRoute()
    {
        SceneManager.LoadScene("RideTown");
    }
}

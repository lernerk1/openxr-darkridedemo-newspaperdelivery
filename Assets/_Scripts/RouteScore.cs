using System;

[Serializable]
public class RouteScore
{
    public int score;
    private (char, char, char) player;
    public string Player => player.ToString();

    public RouteScore(int s, string p)
    {
        score = s;
        player.Item1 = p[0];
        player.Item2 = p[1];
        player.Item3 = p[2];
    }
}

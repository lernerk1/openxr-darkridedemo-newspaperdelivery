using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseTimeTest : MonoBehaviour
{
    private SetUpHands hands;
    
    // Start is called before the first frame update
    void Start()
    {
        hands = SetUpHands.S;
    }

    // Update is called once per frame
    void Update()
    {
        if (hands.RightSelection > 0) Time.timeScale = 0;
        else Time.timeScale = 1;
    }
}

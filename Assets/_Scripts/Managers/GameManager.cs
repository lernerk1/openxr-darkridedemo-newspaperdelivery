using System;
using System.Collections;
using System.Collections.Generic;
using AYellowpaper.SerializedCollections;
using BezierSolution;
using UnityEngine;
using UnityEngine.Rendering.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager S;

    [Header("General")] 
    [SerializedDictionary("Route Name", "Route SO")] 
    public Dictionary<string, BezierSpline[]> routes;
    
    [Header("Player")]
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject truck;
    [SerializeField] private BezierWalkerWithSpeed walker;
    
    // Start is called before the first frame update
    void Start()
    {
        if (S != null)
        {
            Debug.Log("Multiple Game Managers found, Destroying " + gameObject.name);
            Destroy(gameObject);
            return;
        }
        S = this;

        // Get correct route to start
        string routeString = PlayerSettings.Route;
        BezierSpline[] route = routes.ContainsKey(routeString) ? routes[routeString] : routes.Values.GetEnumerator().Current;
        if (route != null)
        {
            walker.spline = route[0];
        }
        else
        {
            Debug.LogWarning("FAILED TO GET FIRST SPLINE FOR PLAYER RIDER");
        }

        // Speed
        walker.speed = PlayerSettings.TravelSpeed;
    }

    public void RouteSegmentCompleted()
    { 
        // Detatch player from rider
        player.transform.parent = null;

        // Ready parcel delivery system
        // TURN ON PARCEL SPAWNING SYSTEM
            // will need to spawn packages in an area at a constant rate into a pile
            // skips to next package with push of button, tetris style
        // TURN ON TRUCK DEPARTURE SYSTEM
            // will need to check if its ready to leave

        // VFX

        // AUDIO
    }

    public void FinalSegmentCompleted()
    {
        // Slow time
        StartCoroutine(nameof(SlowTime), 3f);
        
        // Prepare presentation scene loading
        Invoke(nameof(LoadScorePresentation), 3f);
        
        // VFX
        
        // AUDIO
        
    }

    private void LoadScorePresentation()
    {
        SceneManager.LoadScene("ScorePresentation");
        Time.timeScale = 1;
    }

    private IEnumerator SlowTime(float duration)
    {
        float startTime = Time.time;
        while (Time.timeScale > .5f)
        {
            float u = Mathf.Clamp01((Time.time - startTime) / duration);
            Time.timeScale = Mathf.Lerp(1, .5f, u);
            yield return new WaitForEndOfFrame();
        }
    } 
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager S;
    public TextMeshProUGUI tmpUGUI;

    [field: SerializeField] public uint Score { get; set; }
    
    // Start is called before the first frame update
    void Start()
    {
        if (S == null) S = this;
        else
        {
            Debug.Log("Multiple ScoreManager Initialized, Destroying " + gameObject.name);
            Destroy(gameObject);
        }

        S.Score = 0;
    }

    private void Update()
    {
        if (tmpUGUI != null) tmpUGUI.text = Score.ToString();
    }
}

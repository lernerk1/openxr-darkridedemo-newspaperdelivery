## NOTES
### SAVING DATA
- JUST USE **JSON SERIALIZATION**, ITS BUILT IN, DON'T REDEFINE THE WHEEL WITH THIS ONE
- USE THE RESOURCES FOLDER SINCE THAT GETS INCLUDED IN BUILDS

---

## TO DO
### GREEN LIGHT
- [x] PUT HOLSTER SOMEWHERE THAT MAKES SENSE FOR DESIGN AND FUNCTION
	- [x] BASKET IN FRONT
- [x] MAKE SCORE HOVER NEAR PLAYER'S HEAD (IN FRONT IN TERMS OF TRACK DIRECTION)
	- [ ] OPACITY FADES IF PLAYER NOT LOOKING
- [x] HANDS WITH ANIMATIONS
- [X] NEW ENVIRONMENT (WIDER STREETS, AREAS FOR THE CAR TO STOP AND ALLOW PLAYERS TO WALK AROUND A SMALL AREA)
  - [ ] EXPORT ISLAND INTO UNITY <----------------------------------------------------------------------------------------------------
- [ ] PUT TARGETS IN WINDOWS
- [X] TRACK CHANGES AND GAME MANAGER
	- [X] TRACK START FUNCTIONALITY WITH UNITY EVENT SUPPORT
	- [ ] TRACK COMPLETE MENU FOR RIDETOWN SCENE FOR SCORE DISPLAY AND BUTTONS TO CONTINUE OR RETRY
	- [X] TRACKS HAVE VARIABLE LAP COUNT THEN LOAD A DIFFERENT SCENE ON ALL LAPS COMPLETED
		- [X] TRACKS DON'T LOOP VIA BUILT IN FEATURE, MAKE USE OF ONTRACKCOMPLETE AND GAME MANAGER
- [ ] SCENE MANAGEMENT AND UI
	- [X] MAIN MENU SCENE FOR 
		- [X] PLAY
		- [X] HIGH SCORES
		- [X] CREDITS
		- [X] SETTINGS (TRACK SPEED, COLORS, FOV?, AUDIO)
		- [X] QUIT
	- [X] PLAY MENU
		- [X] TRACK SELECTION
		- [X] LAP COUNT SELECTION
        - [X] BACK BUTTON
	- [ ] PAUSE MENU FOR
		- [X] RESUME
		- [X] RESTART
		- [X] SETTINGS
		- [X] QUIT
        - [ ] SCRIPT FOR KEEPING BUTTON ON WRIST?
          - MODIFIED VERSION OF BILLBOARD? CAN JUST USE BILLBOARD?
	- [ ] RIDE COMPLETED SCENE
		- [ ] SCORE PRESENTATION 
		- [ ] MAIN MENU / RETRY BUTTONS
		- [ ] SETTINGS
		- [ ] QUIT

### YELLOW LIGHT
- [ ] BECOMES A COLOR MATCHING GAME
	- [ ] SET UP TARGET COLOR RANDOMIZATION 
		- TARGETS DO NOT HAVE TO KNOW WHAT COLORS THEIR NEIGHBORS ARE
	- [ ] SEEDED HOLSTER SPAWNING OF DIFFERENT COLORED PREFABS FOR DIFFERENT TARGET COLORS
	- [ ] LOSE POINTS FOR INCORRECT COLOR DELIVERY
	- [ ] SPAWNING TIED TO TRACK PROGRESS
		- X SPAWN ATTEMPTS PER LAP
- [ ] IN RIDE UI FOR 
	- [ ] NEXT COLOR OF MAIL
		- THINK TETRIS
	- [ ] TRACK-PROGRESS/MINI-MAP
	- [ ] JUICED UP SCORE UI
- [X] TWO ADDITIONAL TRACKS
	- [X] GARDENS AREA
	- [X] CROSS TOWN

### RED LIGHT
- [ ] MUSIC AND AMBIENT NOISE
- [ ] VFX AND SFX
	- [ ] NEWSPAPERS
		- [ ] GRAB
		- [ ] RELEASE
		- [ ] SCORE
		- [ ] FAIL
	- [ ] HOLSTER
		- [ ] USE
		- [ ] REFRESH
	- [ ] TRACK COMPLETE
- [ ] DIFFICULTY MODIFIERS (HEAT)
	- [ ] COLORS (RANGE 1-6, DEFAULT 1)
	- [ ] MUST HIT ALL TARGETS (LOSE HEALTH FOR EACH TARGET THAT GETS TOO FAR BEHIND PLAYER)
	- [ ] MOVING OBSTACLES FOR EXTRA CHALLENGE
	- [ ] VILLAGERS (MOVING TARGET)
	- [ ] BIRDS (MOVING ANTI TARGET, MUST HIT WITH OPPOSITE COLOR TO BIRD)
	- [ ] REVERSE TRACK
	- [ ] MIRROR WORLD
	- [ ] USE SLINGSHOT
	- [ ] LIGHTS OUT (ONLY SEE TARGETS, HANDS, AND MAIL)
	- [ ] DELAYED TARGET SELECTION (HIDE TARGET HIGHLIGHTS UNTIL CLOSE RANGE TO PLAYER)

### JUICE PASS
- [ ] COLLABORATE WITH SOMEONE FOR ACTUAL ART ASSETS, FINALLY DO PACKAGE DELIVERY THEME
	- [ ] DIFFERENT SIZE PACKAGES WITH DIFFERENT WEIGHT FOR DIFFERENT SCORE VALUES
	- [ ] MAIL PICK UP TRUCK?
	- [ ] HAND MODELS
	- [ ] NON-TOWNSCAPER ENVIRONMENTS
		- [ ] MAIN MENU AREA
		- [ ] SCORE SUMMARY AREA
		- [ ] ACTUAL DELIVERY ROUTES
- [ ] COLLABORATE WITH SOMEONE FOR BETTER AUDIO ASSETS
- [ ] COLLABORATE WITH SOMEONE FOR BETTER LEVEL DESIGN CHOICES
	- LEARN FROM THEM
- [ ] EXPLORE THE ENVIRONMENT MODE (FPS CONTROLLER TO EXPLORE FULL ISLAND OFF RAILS, NO SCORING SYSTEM)